#See here for info on this module:  https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/
module "VPC_NAME" {   #CHANGE THIS
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.21.0"   #CHECK ON THIS AT THE LINK ABOVE
  name = "utility"   #CHANGE THIS
  cidr = "10.10.0.0/16"
  #Pay attention to which AZs you are specifying here based on region
  azs                 = ["us-east-2a", "us-east-2b", "us-east-2c"]
  private_subnets     = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  public_subnets      = ["10.10.11.0/24", "10.10.12.0/24", "10.10.13.0/24"]
  #database_subnets    = ["10.10.21.0/24", "10.10.22.0/24", "10.10.23.0/24"]
  #elasticache_subnets = ["10.10.31.0/24", "10.10.32.0/24", "10.10.33.0/24"]
  #redshift_subnets    = ["10.10.41.0/24", "10.10.42.0/24", "10.10.43.0/24"]
  create_database_subnet_group = true
  enable_nat_gateway = true
  #enable_vpn_gateway = true
  #enable_s3_endpoint       = true
  #enable_dynamodb_endpoint = true
  enable_dns_support    = true
  enable_dns_hostnames  = true
  enable_dhcp_options   = true
  tags = {
    Project       = "Utility"
  }
}
