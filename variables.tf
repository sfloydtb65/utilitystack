########################## Authentication variables ###########################
variable "region" {
    default = "us-east-2"
}
#The following can be found in 1Password under the Terrform user for the tb-utility account:
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}
